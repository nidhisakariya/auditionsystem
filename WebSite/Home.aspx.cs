﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Home : System.Web.UI.Page
{
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);
            com = new SqlCommand("RetrieveInfo", con);
            con.Open();

            SqlDataReader dr = com.ExecuteReader();

            while (dr.Read())
            {
                startdate.Text = dr["startDate"].ToString();
                enddate.Text = dr["endDate"].ToString();
                resultdate.Text = dr["resultDate"].ToString();
                lblformat.Text = dr["videoFormat"].ToString();
                lblduration.Text = dr["duration"].ToString();
            }
            con.Close();
         }
        catch (Exception exe)
        {
            lblduration.Text = "Error!" + exe;
        }
    }
}