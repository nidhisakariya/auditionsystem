﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ContestantVideo : System.Web.UI.Page
{
    byte vid,cid;
    byte judgeid;
    string user;
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(strConnString);
       // con.ConnectionString = "Data Source=localhost\\SQLExpress;Initial Catalog=AuditionSystem;Integrated Security=True";
        try
        {
            cid = Convert.ToByte(Request.QueryString["ContestantId"]);
            user = Session["username"].ToString();
        }
        catch (NullReferenceException )
        {
            Response.Redirect("Login.aspx");
        }

        try
        {
            SqlCommand com = new SqlCommand("GetJudgeId", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = user;

            IDbDataParameter jid = com.CreateParameter();
            jid.ParameterName = "@id";
            jid.Direction = System.Data.ParameterDirection.Output;
            jid.DbType = System.Data.DbType.Byte;
            com.Parameters.Add(jid);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();

            judgeid = Convert.ToByte(jid.Value);

            string q="select VideoId from Tbl_Videos where LinkToContestantId='" + cid + "'";
            SqlCommand cmd1 = new SqlCommand(q,con);
            con.Open();
            SqlDataReader dr=cmd1.ExecuteReader();
            while(dr.Read())
            {
                vid = Convert.ToByte(dr["VideoId"]);
            }
            con.Close();
            

            SqlCommand cmd = new SqlCommand("CheckExistence", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@JudgeId", SqlDbType.TinyInt)).Value = judgeid;
            cmd.Parameters.Add(new SqlParameter("@VideoId", SqlDbType.TinyInt)).Value = vid;

            IDbDataParameter cnt = cmd.CreateParameter();
            cnt.ParameterName = "@Count";
            cnt.Direction = System.Data.ParameterDirection.Output;
            cnt.DbType = System.Data.DbType.Byte;
            cmd.Parameters.Add(cnt);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            int count = Convert.ToByte(cnt.Value);

            if (count != 0)
            {
                Expression.ReadOnly = true;
                Lipsink.ReadOnly = true;
                Energy.ReadOnly = true;
                Sharpness.ReadOnly = true;
                Gracefulness.ReadOnly = true;
                Steps.ReadOnly = true;
                Reviews.ReadOnly = true;
                Submit.Visible = false;
                lbl.Text = "You have already done the judgement for this Contestant!";
            }
        }
        catch (Exception exe)
        {
            lbl.Text = "Error!" + exe;
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);

            byte Exp = Convert.ToByte(Expression.Text);
            byte Lip = Convert.ToByte(Lipsink.Text);
            byte Enrg = Convert.ToByte(Energy.Text);
            byte Spness = Convert.ToByte(Sharpness.Text);
            byte Grce = Convert.ToByte(Gracefulness.Text);
            byte Stps = Convert.ToByte(Steps.Text);
            byte avg = Convert.ToByte((Exp + Lip + Enrg + Spness + Grce + Stps) / 6);

            SqlCommand cmd = new SqlCommand("AddResults", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Expressions", SqlDbType.TinyInt)).Value = Exp;
            cmd.Parameters.Add(new SqlParameter("@Lipsink", SqlDbType.TinyInt)).Value = Lip;
            cmd.Parameters.Add(new SqlParameter("@Energy", SqlDbType.TinyInt)).Value = Enrg;
            cmd.Parameters.Add(new SqlParameter("@Sharpness", SqlDbType.TinyInt)).Value = Spness;
            cmd.Parameters.Add(new SqlParameter("@Gracefulness", SqlDbType.TinyInt)).Value = Grce;
            cmd.Parameters.Add(new SqlParameter("@Steps", SqlDbType.TinyInt)).Value = Stps;
            cmd.Parameters.Add(new SqlParameter("@Average", SqlDbType.TinyInt)).Value = avg;
            cmd.Parameters.Add(new SqlParameter("@Reviews", SqlDbType.VarChar)).Value = Reviews.Text;
            cmd.Parameters.Add(new SqlParameter("@JudgeId", SqlDbType.TinyInt)).Value = judgeid;
            cmd.Parameters.Add(new SqlParameter("@VideoId", SqlDbType.TinyInt)).Value = vid;

            con.Open();
            int k = cmd.ExecuteNonQuery();
            if (k >= 1)
            {
                lbl.Text = "Points inserted!";
            }
            con.Close();
        }
        catch (Exception ex)
        {
            lbl.Text = "Error!" + ex;
        }

    }
}