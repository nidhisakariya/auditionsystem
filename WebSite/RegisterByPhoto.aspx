﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegisterByPhoto.aspx.cs" Inherits="RegisterByPhoto" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
	    		<h3>REGISTRATION</h3>
                    <center>
                    <form id="form1" runat="server">
                        Upload your photograph:<br />
                        <asp:Image id="img" ImageUrl="images/user.jpg" runat="server" Height="75px" Width="75px" /><br />
                        <asp:FileUpload ID="imgupload" runat="server" Enabled="true" /><br /><br />

                        Aadhar Card photocopy:<br />
                        <asp:Image id="Image1" ImageUrl="images/aadhar.jpg" runat="server" Height="75px" Width="75px" /><br />
                        <asp:FileUpload ID="aadharupload" runat="server" Enabled="true" /><br /><br />
                        <asp:Button ID="upload" runat="server" Text="Upload" OnClick="upload_Click"></asp:Button>
                        <asp:Button ID="reset" runat="server" Text="Reset"></asp:Button>
                        <asp:Label ID="lbl" runat="server" Text=" "></asp:Label>
                    </form>
                   </center>
            </div>
         </div>
     </section>
</body>
</html>
