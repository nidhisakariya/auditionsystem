﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public partial class RegisterJudge : System.Web.UI.Page
{
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    public void Bind_cmb_State()
    {
        try
        {
            SqlConnection conn = new SqlConnection(strConnString);
            conn.Open();
            com = new SqlCommand("select * from Tbl_State", conn);
            SqlDataReader dr = com.ExecuteReader();
            ddlstate.DataSource = dr;
            ddlstate.Items.Clear();
            ddlstate.Items.Add("--Please Select state--");
            ddlstate.DataTextField = "StateName";
            ddlstate.DataValueField = "StateId";
            ddlstate.DataBind();
            conn.Close();
        }
        catch (Exception exe)
        {
            lbl1.Text = exe.ToString();
        }
    }
    public void Bind_City()
    {
        try
        {
            int sid = Convert.ToInt32(ddlstate.SelectedIndex);
            SqlConnection conn = new SqlConnection(strConnString);
            conn.Open();
            com = new SqlCommand("select CityName,CityId from Tbl_City where LinkToStateId =" + sid, conn);
            SqlDataReader dr = com.ExecuteReader();
            ddlcity.DataSource = dr;
            ddlcity.Items.Clear();
            ddlcity.Items.Add("--Please Select city--");
            ddlcity.DataTextField = "CityName";
            ddlcity.DataValueField = "CityId";
            ddlcity.DataBind();
            conn.Close();
        }
        catch (Exception exe)
        {
            lbl1.Text = exe.ToString();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind_cmb_State();
        }  
    }
    protected void submit_Click(object sender, EventArgs e)
    {
        int cnt=0;
        int g = -1;
        if (Male1.Checked == true)
        {
            g = 0;
        }
        else if (Female1.Checked == true)
        {
            g = 1;
        }


        String i = " ";
        if (contemporary1.Checked == true)
        {
            i += " Contemporary";
        }
        if (hiphop1.Checked == true)
        {
            i += " Hiphop";
        }
        if (freestyle1.Checked == true)
        {
            i += " Freestyle";
        }
        if (robotic1.Checked == true)
        {
            i += " Robotic";
        }
        if (classical1.Checked == true)
        {
            i += " Classical";
        }
        if (other1.Checked == true)
        {
            i += " Other";
        }

       string link;
       string fileName = ProfileImage.FileName;
       string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();
       if (FileExtension == "jpeg" || FileExtension == "png" || FileExtension == "jpg")
       {
           string path = Path.GetFileName(ProfileImage.PostedFile.FileName);
           ProfileImage.SaveAs(Server.MapPath("~/JudgeProfilePictures/") + path);
           link = "JudgeProfilePictures/" + path;
           try
           {
               SqlConnection con = new SqlConnection(strConnString);
               com = new SqlCommand("RegisterJudge", con);
               com.CommandType = CommandType.StoredProcedure;
               com.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar)).Value = txt_name1.Text.ToString();
               com.Parameters.Add(new SqlParameter("@Contactno", SqlDbType.VarChar)).Value = txt_cnno1.Text.ToString();
               com.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar)).Value = txt_address1.Text.ToString();
               com.Parameters.Add(new SqlParameter("@dob", SqlDbType.Date)).Value = txt_dob1.Text;
               com.Parameters.Add(new SqlParameter("@City", SqlDbType.TinyInt)).Value = Convert.ToByte(ddlcity.SelectedValue);
               com.Parameters.Add(new SqlParameter("@State", SqlDbType.TinyInt)).Value = Convert.ToByte(ddlstate.SelectedValue);
               com.Parameters.Add(new SqlParameter("@gender", SqlDbType.Bit)).Value = Convert.ToBoolean(g);
               com.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = txt_email1.Text.ToString();
               com.Parameters.Add(new SqlParameter("@interest", SqlDbType.VarChar)).Value = i.ToString();
               com.Parameters.Add(new SqlParameter("@photo", SqlDbType.VarChar)).Value = link;
               com.Parameters.Add(new SqlParameter("@desc", SqlDbType.VarChar)).Value = desc.Text.ToString();
               con.Open();
               int k = com.ExecuteNonQuery();
               if (k != 0)
               {
                   cnt++;
               }
               con.Close();

               SqlCommand cmd = new SqlCommand("UserRegistration", con);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@user", SqlDbType.VarChar)).Value = txt_email1.Text.ToString();
               cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar)).Value = password1.Text.ToString();
               cmd.Parameters.Add(new SqlParameter("@type", SqlDbType.VarChar)).Value = "Judge";
               con.Open();
               int k1 = cmd.ExecuteNonQuery();
               if (k1 != 0)
               {
                   cnt++;
               }
               con.Close();

               if (cnt == 2)
               {
                   lbl1.Text = "Registered Successfully!";
               }
           }
           catch (Exception exe)
           {
               lbl1.Text = exe.ToString();
           }
       }
       else
       {
           lbl1.Text = "The image file is invalid!";
       }
        txt_name1.Text = " ";
        txt_email1.Text = " ";
        txt_cnno1.Text = " ";
        txt_address1.Text = " ";
        desc.Text = " ";
        ddlcity.SelectedIndex = -1;
        ddlstate.SelectedIndex = -1;
        txt_dob1.Text = " ";
        if (contemporary1.Checked == true)
        {
            contemporary1.Checked = false;
        }
        if (hiphop1.Checked == true)
        {
            hiphop1.Checked = false;
        }
        if (freestyle1.Checked == true)
        {
            freestyle1.Checked = false;
        }
        if (robotic1.Checked == true)
        {
            robotic1.Checked = false;
        }
        if (classical1.Checked == true)
        {
            classical1.Checked = false;
        }
        if (other1.Checked == true)
        {
            other1.Checked = false;
        }
        if (Male1.Checked == true)
        {
            Male1.Checked = false;
        }
        else if (Female1.Checked == true)
        {
            Female1.Checked = false;
        }
    }

    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_City();
    }
    protected void reset1_Click(object sender, EventArgs e)
    {
        txt_name1.Text = " ";
        txt_email1.Text = " ";
        txt_cnno1.Text = " ";
        txt_address1.Text = " ";
        desc.Text = " ";
        ddlcity.SelectedIndex = -1;
        ddlstate.SelectedIndex = -1;
        txt_dob1.Text = " ";
        if (contemporary1.Checked == true)
        {
            contemporary1.Checked = false;
        }
        if (hiphop1.Checked == true)
        {
            hiphop1.Checked = false;
        }
        if (freestyle1.Checked == true)
        {
            freestyle1.Checked = false;
        }
        if (robotic1.Checked == true)
        {
            robotic1.Checked = false;
        }
        if (classical1.Checked == true)
        {
            classical1.Checked = false;
        }
        if (other1.Checked == true)
        {
            other1.Checked = false;
        }
        if (Male1.Checked == true)
        {
            Male1.Checked = false;
        }
        else if (Female1.Checked == true)
        {
            Female1.Checked = false;
        }
    }
    static string Encrypt(string value)
    {
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(value));
            return Convert.ToBase64String(data);
        }
    }
    protected void password1_TextChanged(object sender, EventArgs e)
    {
        password1.Text = Encrypt(password1.Text);
    }
    protected void retypepassword1_TextChanged(object sender, EventArgs e)
    {
        retypepassword1.Text = Encrypt(retypepassword1.Text);
    }
}