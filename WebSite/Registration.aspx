﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
	    		<h3>REGISTRATION</h3>
                    <center>
                    <form id="form1" runat="server">
                        <asp:Label ID="lbl1" Text=" " runat="server" ForeColor="Red"></asp:Label><br />
                        Name:
                        <asp:TextBox ID="txt_name" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="Required!" ForeColor="Red"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red" ErrorMessage="Invalid name" ControlToValidate="txt_name" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator><br />

                        Contact No:
                        <asp:TextBox ID="txt_cnno" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_cnno" ErrorMessage="Required!" ForeColor="Red"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_cnno" ForeColor="Red" ValidationExpression="[0-9]{10}" ErrorMessage="Please enter 10 digit contact no."></asp:RegularExpressionValidator><br />

                        Address:
                        <asp:TextBox ID="txt_address" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox><br />

                        Select State:
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                       <asp:UpdatePanel ID="StatePanel" runat="server">
                           <ContentTemplate>
                                 <asp:DropDownList ID="cmb_state"  Width="400px"  AutoPostBack ="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="cmb_state_SelectedIndexChanged"></asp:DropDownList><br />
                            </ContentTemplate> 
                       </asp:UpdatePanel>
                            
                        Select City:
                        <asp:UpdatePanel ID="CityPanel" runat="server">
                           <ContentTemplate>
                              <asp:DropDownList ID="cmb_city" Width="400px" AppendDataBoundItems="true" runat="server"></asp:DropDownList><br />
                             </ContentTemplate>
                       </asp:UpdatePanel>
                        
                        Gender:
                        <asp:RadioButton ID="Male" Text="Male" runat="server" GroupName="gender"></asp:RadioButton>
                        <asp:RadioButton ID="Female" Text="Female" runat="server" GroupName="gender"></asp:RadioButton><br />
                        
                        Date of Birth:
                        <asp:TextBox ID="dob" TextMode="Date" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  ForeColor="Red" runat="server" ControlToValidate="dob" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                       
                        Email Id:
                        <asp:TextBox ID="txt_email" Width="400px" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" ControlToValidate="txt_email" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_email" runat="server" ForeColor="Red" ErrorMessage="Invalid email" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator><br />

                        Field Of Interest:<br /><br />
                        <asp:CheckBox ID="contemporary" text="Contemporary" runat="server" GroupName="interest"></asp:CheckBox>
                        <asp:CheckBox ID="hiphop" text="Hip-Hop" runat="server" GroupName="interest"></asp:CheckBox>
                        <asp:CheckBox ID="freestyle" text="Freestyle" runat="server" GroupName="interest"></asp:CheckBox>
                        <asp:CheckBox ID="robotic" text="Robotics" runat="server" GroupName="interest"></asp:CheckBox>
                        <asp:CheckBox ID="classical" text="Classical" runat="server" GroupName="interest"></asp:CheckBox>
                        <asp:CheckBox ID="other" text="Other" runat="server" GroupName="interest"></asp:CheckBox><br /><br />
                         
                        Aadhar Card No:
                        <asp:TextBox ID="Ano" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  ForeColor="Red" runat="server" ControlToValidate="Ano" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ForeColor="Red" ControlToValidate="Ano" ErrorMessage="Invalid aadhar no" ValidationExpression="^\d{4}\s\d{4}\s\d{4}$"></asp:RegularExpressionValidator><br />
                        
                        Upload photo:<br />
                        <asp:FileUpload ID="ImageUpload" runat="server"></asp:FileUpload><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" ControlToValidate="ImageUpload" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Button ID="submit" runat="server" Text="Submit" onclick="submit_Click"></asp:Button>
                        <asp:Button ID="reset" runat="server" Text="Reset" onclick="reset_Click"></asp:Button><br />
                        <asp:Label ID="lbl" runat="server" Text=" "></asp:Label>
                        </form>
                   </center>
            </div>
         </div>
     </section>
      <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>
