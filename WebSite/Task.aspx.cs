﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class Task : System.Web.UI.Page
{
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    byte uid;
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            n = Session["username"].ToString();
        }
        catch(NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }

        SqlConnection con = new SqlConnection(strConnString);
        try
        {
            com = new SqlCommand("GetDetail", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = n;

            IDbDataParameter cid = com.CreateParameter();
            cid.ParameterName = "@id";
            cid.Direction = System.Data.ParameterDirection.Output;
            cid.DbType = System.Data.DbType.Byte;
            com.Parameters.Add(cid);

            IDbDataParameter cnum = com.CreateParameter();
            cnum.ParameterName = "@num";
            cnum.Direction = System.Data.ParameterDirection.Output;
            cnum.DbType = System.Data.DbType.String;
            cnum.Size = 50;
            com.Parameters.Add(cnum);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();

            uid = Convert.ToByte(cid.Value);
            userId.Text = uid.ToString();

            com = new SqlCommand("CheckUpload", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ContestantId", SqlDbType.VarChar)).Value = uid;

            IDbDataParameter cnt = com.CreateParameter();
            cnt.ParameterName = "@Count";
            cnt.Direction = System.Data.ParameterDirection.Output;
            cnt.DbType = System.Data.DbType.Byte;
            com.Parameters.Add(cnt);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();

            int count = Convert.ToByte(cnt.Value);

            if (count != 0)
            {
                lblUpload.Visible = false;
                type.Visible=false;
                VideoUpload.Visible = false;
                Add.Visible=false;
                video.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error! " + ex;
        }
    }
    protected void Add_Click(object sender, EventArgs e)
    {
        string linked;
        string fileName = VideoUpload.FileName;
        string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();
        if (FileExtension == "mp4")
        {
            string path = Path.GetFileName(VideoUpload.FileName);
            path = path.Replace(" ", "");
            VideoUpload.SaveAs(Server.MapPath("~/VideoData/") + path);
            string link = "VideoData/" + path;
            linked = "<video width=400 Controls><Source src=" + link + " type=video/mp4></video>";
            try
            {
                SqlConnection con = new SqlConnection(strConnString);

                com = new SqlCommand("VideoUpload", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar)).Value = type.Text;
                com.Parameters.Add(new SqlParameter("@Video", SqlDbType.VarChar)).Value = linked;
                com.Parameters.Add(new SqlParameter("@ContestantId", SqlDbType.TinyInt)).Value = uid;

                con.Open(); ;
                com.ExecuteNonQuery();
                con.Close();

                lblUpload.Visible = false;
                type.Visible = false;
                VideoUpload.Visible = false;
                Add.Visible = false;
                video.Visible = true;
                lblMessage.Visible = false;
                Literal1.Text = linked;
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Error!" + ex;
            }
        }
        else
        {
            lblMessage.Text = "Please provide the file with correct format!";
        }
    } 
}