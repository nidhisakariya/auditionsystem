﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
  <section id="banner">
                <div class="inner">
                    <h1>Dance Out Loud</h1>
                    <p>Dance to the moon and reach to the stars<br /></p>
                </div>
                <video autoplay loop muted playsinline src="images/banner.mp4"></video>
            </section>      
        <section class="wrapper">
                <div class="inner">
                    <header class="special">
                        <h2>AUDITION PROCESS</h2>
                    </header>
                    <div class="highlights">
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-print"><span class="label">Icon</span></a>
                                    <h3>STEP 1:</h3>
                                </header>
                                <p>Register yourself for the show by providing the correct details.</p>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-envelope-o"><span class="label">Icon</span></a>
                                    <h3>STEP 2:</h3>
                                </header>
                                <p>Login with your email and provided password</p>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-file-video-o"><span class="label">Icon</span></a>
                                    <h3>STEP 3:</h3>
                                </header>
                                <p>Upload your mp4 video on or before the deadlines provided.</p>
                            </div>
                        </section>
                    </div>
                    </div>
                    <div class="inner">
                        <header class="special">
                        <h2>AUDITION RULES</h2>
                    </header>
                    <div class="highlights">
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-calendar"><span class="label">Icon</span></a>
                                    <h3>Dates:</h3>
                                </header>
                                <p>The audition process would be conducted from <asp:Label ID="startdate" Forecolor="Red" runat="server" Text=" "></asp:Label> to <asp:Label ID="enddate" Forecolor="Red" runat="server" Text=" "></asp:Label></p>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-camera"><span class="label">Icon</span></a>
                                    <h3>VIDEOS:</h3>
                                </header>
                                <p>The video should be of <asp:Label ID="lblformat" Forecolor="Red" runat="server" Text=" "></asp:Label> format atmost of <asp:Label ID="lblduration" Forecolor="Red" runat="server" Text=" "></asp:Label> minutes.</p>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <header>
                                    <a href="#" class="icon fa-trophy"><span class="label">Icon</span></a>
                                    <h3>JUDGEMENT</h3>
                                </header>
                                <p>Judgement will done on the basis of criterias like dance,expressions,body language and sharpness,and the resuts will be declared on <asp:Label ID="resultdate" Forecolor="Red" runat="server" Text=" "></asp:Label></p>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
             <section class="wrapper">
                <div class="inner">
                    <header class="special">
                        <h2>The Audition Process,</h2>
                        <p>Made Simple Just For You</p>
                    </header>
                    <div class="testimonials">
                        <section>
                            <div class="content">
                                <blockquote>
                                    <p>Feedback</p>
                                </blockquote>
                                <div class="author">
                                    <p class="credit">- <strong>Name</strong> <!<span>Contestant</span>></p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <blockquote>
                                    <p>Feedback</p>
                                </blockquote>
                                <div class="author">
                                    <p class="credit">- <strong>Name</strong> <!<span>Contestant</span>></p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="content">
                                <blockquote>+
                                    <p>Feedback</p>
                                </blockquote>
                                <div class="author">
                                    <p class="credit">- <strong>Name</strong> <!<span></span>Contestants></p>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
       
</asp:Content>

