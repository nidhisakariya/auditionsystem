﻿<%@ Page Title="" Language="C#" MasterPageFile="~/JudgeDashboard.master" AutoEventWireup="true" CodeFile="ContestantVideo.aspx.cs" Inherits="ContestantVideo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="heading" >
		<h1>VIDEOS</h1>
    </div>
    <section id="main" class="wrapper">
    <div class="inner">
			<div class="content">
                    <center>
                    <form id="Cvideo">
                    <asp:Label ID="lbl" runat="server" ForeColor="Red" Text=" "></asp:Label><br />
                    <asp:DataList ID="DataList1" runat="server" DataKeyField="VideoId" 
                        DataSourceID="SqlDataSource1" RepeatColumns="1">
                        <ItemTemplate>
                            <table class="style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" Align="Center" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Video") %>'></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString9 %>" 
                        SelectCommand="SELECT * FROM [Tbl_Videos] WHERE ([LinkToContestantId] = @LinkToContestantId)">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="LinkToContestantId" QueryStringField="ContestantId" 
                                Type="Byte" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                    <br />
                    Expressions:
                    <asp:TextBox ID="Expression" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Expression" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                   <asp:RangeValidator ID="RangeValidator1" runat="server" Type="Integer" ControlToValidate="Expression" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" MaximumValue="10" MinimumValue="1"></asp:RangeValidator><br />
                    Lipsink:
                    <asp:TextBox ID="Lipsink" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Lipsink" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="RangeValidator2" runat="server" Type="Integer" ControlToValidate="Lipsink" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" MaximumValue="10" MinimumValue="1"></asp:RangeValidator><br />
                    
                    Energy:
                    <asp:TextBox ID="Energy" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="Energy" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="RangeValidator3" runat="server" Type="Integer" ControlToValidate="Energy" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" MaximumValue="10" MinimumValue="1"></asp:RangeValidator><br />
                    

                    Sharpness:
                    <asp:TextBox ID="Sharpness" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="Sharpness" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="Sharpness" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" MaximumValue="10" Type="Integer"  MinimumValue="1"></asp:RangeValidator><br />
                    
                    Gracefulness:
                    <asp:TextBox ID="Gracefulness" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="Gracefulness" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="Gracefulness" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" Type="Integer" MaximumValue="10" MinimumValue="1"></asp:RangeValidator><br />
                    
                    Steps:
                    <asp:TextBox ID="Steps" Align="Center" Width="400" runat="server"></asp:TextBox><br />
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="Steps" runat="server" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="Steps" ForeColor="Red" ErrorMessage="Marks should be between 1 to 10" MaximumValue="10" Type="Integer" MinimumValue="1"></asp:RangeValidator><br />
                    
                    Reviews:
                    <asp:TextBox ID="Reviews" Align="Center" Width="400" TextMode="MultiLine" runat="server"></asp:TextBox><br />
                    
                    <br />
                    <asp:Button Id="Submit" Align="Center" Text="Submit" runat="server" 
                        onclick="Submit_Click" />
                    <br />
                     </form>
                   </center>
                </div>
        </div> 
        </section>

</asp:Content>

