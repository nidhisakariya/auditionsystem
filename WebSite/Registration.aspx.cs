﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;

public partial class Registration : System.Web.UI.Page
{
    string ans;

    public void sendsms(string content, string number)
    {
        string destinationaddr = "91" + number;
        string message = "Hey dancer! Your OTP for registering with Dance Out Loud is" + content;

        String message1 = HttpUtility.UrlEncode(message);
        using (var wb = new WebClient())
        {
            byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "UXj3X4D66II-R6WeadtXjuGpGi0Nr45eDqL4ItX5ES"},
                {"numbers" , destinationaddr},
                {"message" , message1},
                {"sender" , "TXTLCL"}
                });
            string result = System.Text.Encoding.UTF8.GetString(response);
        }
    }
    public void sendEmail(string content, string mailaddress)
    {
        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        mail.To.Add(mailaddress);
        mail.From = new MailAddress("email@gmail.com", "Audition Magic", System.Text.Encoding.UTF8);
        mail.Subject = "OTP for Dance Out Loud";
        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        mail.Body = "Hey dancer! Your OTP to register with Dance Out Loud is:" + content;
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential("email@gmail.com", "password");
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception ex)
        {
            Exception ex2 = ex;
            string errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }
        }
    }
    public String otpgeneration()
    {
        string numbers = "1234567890";
        string characters = numbers;
        int length = 6;
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    public void Bind_cmb_State()
    {
         try 
         {
            SqlConnection conn = new SqlConnection(strConnString);
            conn.Open();
            com = new SqlCommand("select * from Tbl_State", conn);
            SqlDataReader dr = com.ExecuteReader();
            cmb_state.DataSource = dr;
            cmb_state.Items.Clear();
            cmb_state.Items.Add("--Please Select state--");
            cmb_state.DataTextField = "StateName";
            cmb_state.DataValueField = "StateId";
            cmb_state.DataBind();
            conn.Close();
         }
         catch(Exception exe)
         {
            lbl.Text = exe.ToString();
         }
    }
    public void Bind_City()
    {
        try
        {
            int sid = Convert.ToInt32(cmb_state.SelectedIndex );
            SqlConnection conn = new SqlConnection(strConnString);
            conn.Open();
            com = new SqlCommand("select CityName,CityId from Tbl_City where LinkToStateId =" + sid, conn);
            SqlDataReader dr = com.ExecuteReader();
            cmb_city.DataSource = dr;
            cmb_city.Items.Clear();
            cmb_city.Items.Add("--Please Select city--");
            cmb_city.DataTextField = "CityName";
            cmb_city.DataValueField = "CityId";
            cmb_city.DataBind();  
            conn.Close();
        }
        catch (Exception exe)
        {
            lbl.Text = exe.ToString();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind_cmb_State();
        }  
    }
    protected void submit_Click(object sender, EventArgs e)
    {
        DateTime d = Convert.ToDateTime(dob.Text);
        TimeSpan tm = (DateTime.Now - d);
        int age = (tm.Days / 365);

        if (age <= 30 && age >= 18)
        {
            int g = -1;
            if (Male.Checked == true)
            {
                g = 0;
            }
            else if (Female.Checked == true)
            {
                g = 1;
            }


            String i = " ";
            if (contemporary.Checked == true)
            {
                i += " Contemporary";
            }
            if (hiphop.Checked == true)
            {
                i += " Hiphop";
            }
            if (freestyle.Checked == true)
            {
                i += " Freestyle";
            }
            if (robotic.Checked == true)
            {
                i += " Robotic";
            }
            if (classical.Checked == true)
            {
                i += " Classical";
            }
            if (other.Checked == true)
            {
                i += " Other";
            }

            string link;
            string fileName = ImageUpload.FileName;
            string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();
            if (FileExtension == "jpeg" || FileExtension == "png" || FileExtension=="jpg")
            {
                string path = Path.GetFileName(ImageUpload.PostedFile.FileName);
                ImageUpload.SaveAs(Server.MapPath("~/ProfilePictures/") + path);
                link = "ProfilePictures/" + path;
                    try
                    {
                        SqlConnection con = new SqlConnection(strConnString);
                        com = new SqlCommand("RegisterContestant", con);
                        com.CommandType = CommandType.StoredProcedure;
                        com.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar)).Value = txt_name.Text.ToString();
                        com.Parameters.Add(new SqlParameter("@Contactno", SqlDbType.VarChar)).Value = txt_cnno.Text.ToString();
                        com.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar)).Value = txt_address.Text.ToString();
                        com.Parameters.Add(new SqlParameter("@City", SqlDbType.TinyInt)).Value = Convert.ToByte(cmb_city.SelectedValue);
                        com.Parameters.Add(new SqlParameter("@State", SqlDbType.TinyInt)).Value = Convert.ToByte(cmb_state.SelectedValue);
                        com.Parameters.Add(new SqlParameter("@gender", SqlDbType.Bit)).Value = Convert.ToBoolean(g);
                        com.Parameters.Add(new SqlParameter("@dob", SqlDbType.Date)).Value = Convert.ToDateTime(dob.Text);
                        com.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = txt_email.Text.ToString();
                        com.Parameters.Add(new SqlParameter("@interest", SqlDbType.VarChar)).Value = i.ToString();
                        com.Parameters.Add(new SqlParameter("@aadharno", SqlDbType.VarChar)).Value = Ano.Text.ToString();
                        com.Parameters.Add(new SqlParameter("@photo", SqlDbType.VarChar)).Value = link;

                        ans = otpgeneration();
                        //sendEmail(ans, txt_email.Text);
                        sendsms(ans,txt_cnno.Text);
                        Session["otp"] = ans.ToString();

                        con.Open();
                        int k = com.ExecuteNonQuery();
                        if (k != 0)
                        {
                            Session["email"] = txt_email.Text;
                            Response.Redirect("Verification.aspx");
                        }
                        con.Close();
                    }
                    catch (Exception exe)
                    {
                        lbl.Text = exe.ToString();
                    }
            }
            else
            {
                lbl1.Text = "Please upload the correct image file";
            }
        }
        else
        {
            lbl1.Text = "Sorry! The age validatidity for the show is 18 to 30";
        }
        txt_name.Text = " ";
        txt_email.Text = " ";
        txt_cnno.Text = " ";
        txt_address.Text = " ";
        Ano.Text = " ";
        cmb_city.SelectedIndex = -1;
        cmb_state.SelectedIndex = -1;
        dob.Text = " ";
        if (contemporary.Checked == true)
        {
            contemporary.Checked = false;
        }
        if (hiphop.Checked == true)
        {
            hiphop.Checked = false;
        }
        if (freestyle.Checked == true)
        {
            freestyle.Checked = false;
        }
        if (robotic.Checked == true)
        {
            robotic.Checked = false;
        }
        if (classical.Checked == true)
        {
            classical.Checked = false;
        }
        if (other.Checked == true)
        {
            other.Checked = false;
        }
        if (Male.Checked == true)
        {
            Male.Checked = false;
        }
        else if (Female.Checked == true)
        {
            Female.Checked = false;
        }
    }
    protected void cmb_state_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind_City();
    }
    protected void reset_Click(object sender, EventArgs e)
    {
        txt_name.Text = " ";
        txt_email.Text = " ";
        txt_cnno.Text = " ";
        txt_address.Text = " ";
        Ano.Text = " ";
        cmb_city.SelectedIndex = -1;
        cmb_state.SelectedIndex = -1;
        dob.Text = " ";
        if (contemporary.Checked == true)
        {
            contemporary.Checked = false;
        }
        if (hiphop.Checked == true)
        {
            hiphop.Checked = false;
        }
        if (freestyle.Checked == true)
        {
            freestyle.Checked = false;
        }
        if (robotic.Checked == true)
        {
            robotic.Checked = false;
        }
        if (classical.Checked == true)
        {
            classical.Checked = false;
        }
        if (other.Checked == true)
        {
            other.Checked = false;
        }
        if (Male.Checked == true)
        {
            Male.Checked = false;
        }
        else if (Female.Checked == true)
        {
            Female.Checked = false;
        }
    }
}