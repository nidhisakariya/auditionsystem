﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProfilePage : System.Web.UI.Page
{
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            n = Session["username"].ToString();
        }
        catch (NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }
    }
}