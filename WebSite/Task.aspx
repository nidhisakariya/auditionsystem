﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Task.aspx.cs" Inherits="Task" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="heading" >
		<h1>TASK</h1>
    </div>
    <section id="main" class="wrapper">
    <div class="inner">
			<div class="content">
                    <center>
                    <form id="task">
                        <br />
                        <asp:Label ID="video" Text="Your video:" runat="server" Visible="false" Font-Bold="True" Font-Size="36"></asp:Label>
                        <br />
                        <br />
                        <asp:Label Text="Upload your video here:" ID="lblUpload" runat="server" Font-Bold="True" Font-Size="36"></asp:Label> 
                        <br />
                        <asp:DataList ID="DataList1" runat="server" DataKeyField="VideoId" 
                            DataSourceID="SqlDataSource1">
                            <ItemTemplate>
                                <asp:Literal ID="Literal2" runat="server" 
    Text='<%# Eval("Video") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:DataList>
                        <br />
                        <asp:TextBox ID="type" Width="400px" placeholder="Dance Form" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="type" ForeColor="Red" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                        &nbsp <asp:FileUpload ID="VideoUpload" runat="server"></asp:FileUpload><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="VideoUpload" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                        <asp:Button ID="Add" runat="server" Text="Add" onclick="Add_Click"></asp:Button>
                        <br />
                        <asp:Label ID="lblMessage" Forecolor="Red" runat="server" Text=" "></asp:Label>
                        <asp:Label ID="userId" runat="server" Visible="false" Text=" "></asp:Label>
                        <br />
                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        <br />
                        <br />
                        <br />
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString %>" 
                            SelectCommand="SELECT * FROM [Tbl_Videos] WHERE ([LinkToContestantId] = @LinkToContestantId)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="userId" Name="LinkToContestantId" 
                                    PropertyName="Text" Type="Byte" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <br /> 
                        <br />
                   </form>
                   </center>
                </div>
        </div> 
        </section>
</asp:Content>

