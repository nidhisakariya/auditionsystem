﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="judge.aspx.cs" Inherits="judge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <div id="heading" >
		<h1>ABOUT JUDGES</h1>
    </div>
    <section id="main" class="wrapper">
    <div class="inner">
			<div class="content">
                    <center>
                    <form id="judge">
                    <br />
                    <br />
                    <br />
                    <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1" 
                        RepeatColumns="3" RepeatDirection="Horizontal">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("JudgeName") %>'></asp:Label>
                            <br />
                            <asp:Image ID="Image1" Width="400px" Height="500px" runat="server" ImageUrl='<%# Eval("Photograph") %>' />
                            <br />
                            <asp:Label ID="Label2" Width="400px" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                            <br />
                            Expertise in:<br />
                            <asp:Label ID="Label3" Width="400px" runat="server" Text='<%# Eval("FieldMastery") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                    <br />
                    <br />
                    <br />
                    </form>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString5 %>" 
                            SelectCommand="SELECT [JudgeName], [FieldMastery], [Photograph], [Description] FROM [Tbl_Judge_Master]">
                        </asp:SqlDataSource>
                        <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                   </center>
                </div>
        </div> 
        </section>
</asp:Content>

