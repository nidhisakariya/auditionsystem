﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AssignTask : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CompareValidator2.ValueToCompare = DateTime.Now.ToString("MM/dd/yyyy");
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    protected void Submit_Click(object sender, EventArgs e)
    {
        DateTime dt1 = DateTime.Today;
        DateTime dt2 = Convert.ToDateTime(sdate.Text);
        if (dt1 >= dt2)
        {
            lblDateError.Text = "Date should be greater than today";
        }
        else
        {
            try
            {
                SqlConnection con = new SqlConnection(strConnString);
                com = new SqlCommand("UploadTask", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("@sdate", SqlDbType.Date)).Value = sdate.Text;
                com.Parameters.Add(new SqlParameter("@edate", SqlDbType.Date)).Value = edate.Text;
                com.Parameters.Add(new SqlParameter("@rdate", SqlDbType.Date)).Value = result.Text;
                com.Parameters.Add(new SqlParameter("@duration", SqlDbType.TinyInt)).Value = Convert.ToByte(duration.Text);
                com.Parameters.Add(new SqlParameter("@vformat", SqlDbType.VarChar)).Value = format.Text.ToString();
                com.Parameters.Add(new SqlParameter("@cdate", SqlDbType.Date)).Value = DateTime.Now.ToString("dd-MM-yyyy");
                com.Parameters.Add(new SqlParameter("@ctime", SqlDbType.Time)).Value = DateTime.Now.ToString("hh:mm");
                con.Open();
                int k = com.ExecuteNonQuery();
                if (k != 0)
                {
                    lblMessage.Text = "Information inserted Successfully!";
                }
                con.Close();
            }
            catch (Exception exe)
            {
                lblMessage.Text = "Error!" + exe;
            }
        }
    }
}