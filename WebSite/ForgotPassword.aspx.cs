﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    string ans;
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        ans = otpgeneration();
        sendEmail(ans, email.Text);
        //sendsms(ans,txt_cnno.Text);
        Session["otp"] = ans.ToString();
        Response.Redirect("Verification.aspx");
    }
    public void sendEmail(string content, string mailaddress)
    {
        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        mail.To.Add(mailaddress);
        mail.From = new MailAddress("17bmiit141@gmail.com", "Audition Magic", System.Text.Encoding.UTF8);
        mail.Subject = "OTP for Dance Out Loud";
        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        mail.Body = "Hey dancer! Your OTP to register with Dance Out Loud is:" + content;
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential("17bmiit141@gmail.com", "shreenathji");
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception ex)
        {
            Exception ex2 = ex;
            string errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }
        }
    }
    public String otpgeneration()
    {
        string numbers = "1234567890";
        string characters = numbers;
        int length = 6;
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;
    }
}