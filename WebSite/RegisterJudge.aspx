﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminDashboard.master" AutoEventWireup="true" CodeFile="RegisterJudge.aspx.cs" Inherits="RegisterJudge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
    <h3>REGISTER JUDGE</h3>
                    
                    <form id="formjug" class="form-horizontal" runat="server">
                    <br />
                    <br />
                        Name:
                        <asp:TextBox ID="txt_name1" runat="server" Width="400px" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name1" ErrorMessage="Required!" ForeColor="Red"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red" ErrorMessage="Invalid name" ControlToValidate="txt_name1" ValidationExpression="^[a-zA-Z'.\s]{1,50}"></asp:RegularExpressionValidator><br />

                        Contact no: 
                        <asp:TextBox ID="txt_cnno1" class="form-control" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_cnno1" ErrorMessage="Required!" ForeColor="Red"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_cnno1" ForeColor="Red" ValidationExpression="[0-9]{10}" ErrorMessage="Please enter 10 digit contact no."></asp:RegularExpressionValidator><br />

                        Date Of Birth:
                        <asp:TextBox ID="txt_dob1" class="form-control"  Width="400px" runat="server" TextMode="Date"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="txt_dob1" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                        <br />
                        Address
                        <asp:TextBox ID="txt_address1" runat="server" class="form-control" TextMode="MultiLine" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" ControlToValidate="txt_address1" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />

                        Select State:
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                       <asp:UpdatePanel ID="StatePanel" runat="server">
                           <ContentTemplate>
                                 <asp:DropDownList ID="ddlstate" class="form-control"  Width="400px"  AutoPostBack ="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged"></asp:DropDownList><br />
                            </ContentTemplate> 
                       </asp:UpdatePanel>
                             
                        Select City:
                        <asp:UpdatePanel ID="CityPanel" runat="server">
                           <ContentTemplate>
                              <asp:DropDownList ID="ddlcity" Width="400px" class="form-control" AppendDataBoundItems="true" runat="server"></asp:DropDownList><br />
                             </ContentTemplate>
                       </asp:UpdatePanel>
                        
                        Gender:<br />
                        <asp:RadioButton ID="Male1" Text="Male" runat="server" GroupName="gen"></asp:RadioButton>
                        &nbsp;
                        <asp:RadioButton ID="Female1" Text="Female" runat="server" GroupName="gen"></asp:RadioButton><br />
                        <br />
                        Email Id:
                        <asp:TextBox ID="txt_email1" class="form-control" Width="400px" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" ControlToValidate="txt_email1" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_email1" runat="server" ForeColor="Red" ErrorMessage="Invalid email" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator><br />

                        Field Mastery: <br />
                        <asp:CheckBox ID="contemporary1" text="Contemporary" runat="server" GroupName="inte"></asp:CheckBox>
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="hiphop1" text="Hip-Hop" runat="server" GroupName="inte"></asp:CheckBox>
                        &nbsp;
                        <asp:CheckBox ID="freestyle1" text="Freestyle" runat="server" GroupName="inte"></asp:CheckBox>
                        &nbsp;
                        <asp:CheckBox ID="robotic1" text="Robotics" runat="server" GroupName="inte"></asp:CheckBox>
                        &nbsp;
                        <asp:CheckBox ID="classical1" text="Classical" runat="server" GroupName="inte"></asp:CheckBox>
                        &nbsp;
                        <asp:CheckBox ID="other1" text="Other" runat="server" GroupName="inte"></asp:CheckBox><br /><br />
                         
                        Password:
                        <asp:TextBox ID="password1" ForeColor="Red" Textmode="Password" 
                        class="form-control" Width="400px" runat="server" 
                        ontextchanged="password1_TextChanged"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="password1" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator> <br />
                       
                        Retype password:
                        <asp:TextBox ID="retypepassword1" Textmode="Password" class="form-control" 
                        Width="400px" runat="server" ontextchanged="retypepassword1_TextChanged"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ForeColor="Red" ControlToValidate="retypepassword1"  ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                        <asp:CompareValidator ID="CompareValidator1" ControlToCompare="password1" ControlToValidate="retypepassword1" runat="server" ErrorMessage="Password doesn't match" ForeColor="Red"></asp:CompareValidator> <br />
                    
                        Upload Photo:
                        <asp:FileUpload ID="ProfileImage" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="ProfileImage" ForeColor="Red" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                       
                        Description:
                        <asp:TextBox ID="desc" TextMode="MultiLine" Width="400px" class="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="desc" ForeColor="Red" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Button ID="submit1" runat="server" Text="Submit" OnClick="submit_Click"></asp:Button>
                        <asp:Button ID="reset1" runat="server" Text="Reset" onclick="reset1_Click"></asp:Button><br />
                        <asp:Label ID="lbl1" ForeColor="Red" runat="server" Text=" "></asp:Label>
                        <br />
                        <br />
                        <br />
                        </form>
                   </center>
</asp:Content>
