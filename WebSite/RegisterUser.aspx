﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegisterUser.aspx.cs" Inherits="RegisterUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
	    		<h3>REGISTRATION</h3>
                    <center>
                    <form id="form1" runat="server">
                      <asp:TextBox ID="uname" width="400px" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="uname" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator>
                        
                       <asp:TextBox ID="password" Textmode="Password" placeholder="Password" 
                          Width="400px" runat="server" ontextchanged="password_TextChanged"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="password" ForColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator> <br />
                       <asp:TextBox ID="retypepassword" Textmode="Password" 
                          placeholder="Retype Password" Width="400px" runat="server" 
                          ontextchanged="retypepassword_TextChanged"></asp:TextBox>
                       <asp:CompareValidator ID="CompareValidator1" ControlToCompare="password" ControlToValidate="retypepassword" runat="server" ErrorMessage="Password doesn't match" ForeColor="Red"></asp:CompareValidator> <br />
                         <asp:Button ID="Register1" runat="server" Text="Register" OnClick="Register1_Click"></asp:Button>
                   <asp:Label ID="lbl" runat="server" Text=" "></asp:Label>
                         </form>
                   </center>
            </div>
         </div>
     </section>
     <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>
