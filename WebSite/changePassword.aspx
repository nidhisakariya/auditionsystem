﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="changePassword.aspx.cs" Inherits="changePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: darkred;
  color: white;
}

.topnav-right {
  float: right;
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <div class="topnav">
                     <div class="topnav-right">
                            <a class="active" href="changePassword.aspx">Change Password</a>&nbsp;
                            <a href="EditProfile.aspx">Edit Profile</a>
                     </div>
                    </div>
 <div id="heading" >
				<h1>PROFILE </h1>
    </div>
    <section id="main" class="wrapper">
                    <div class="inner">
			        <div class="content">
                    <center>
                    <form id="profile">
                    <br />
                    <br />
                    CurrentPassword:
                    <asp:TextBox ID="Cpassword" Width="400px" runat="server" TextMode="Password"
                        ontextchanged="Cpassword_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Cpassword" ForeColor="Red"  runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <br />
                    <br />
                    New Password:
                    <asp:TextBox ID="Npassword" Width="400px" runat="server" TextMode="Password"
                        ontextchanged="Npassword_TextChanged"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Npassword" ForeColor="Red" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                    <br />
                    Retype Password:
                    <asp:TextBox ID="rpassword" Width="400px" runat="server" TextMode="Password"
                        ontextchanged="rpassword_TextChanged"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="rpassword" ForeColor="Red" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="CompareValidator1" ControlToValidate="rpassword" ControlToCompare="Npassword" ForeColor="Red" runat="server" ErrorMessage="Password doesn't match!"></asp:CompareValidator>
                    <br />
                    <asp:Button ID="Submit" runat="server" Text="Submit" onclick="Submit_Click"></asp:Button>
                    <br />
                    <asp:Label ID="label" ForeColor="Red" runat="server" Text=" "></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    </form>
                   </center>
                </div>
        </div> 
        </section>
</asp:Content>

