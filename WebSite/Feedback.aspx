﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Feedback.aspx.cs" Inherits="Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="heading" >
		<h1>FEEDBACK</h1>
    </div>
    <section id="main" class="wrapper">
    <div class="inner">
			<div class="content">
                    <center>
                    <form id="feedback">
                    <br />
                    <br />
                    <br />
                    <h3>Your feedbacks are valuable for us,please provide it below</h3>
                    <br />
                    <asp:TextBox ID="txtfeedback" Width="600px" TextMode="MultiLine" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtfeedback" runat="server" ErrorMessage="Required!!"></asp:RequiredFieldValidator>
                    <br />
                    <br />
                    <asp:Button ID="btnFeedback" runat="server" Text="Submit" 
                        onclick="btnFeedback_Click"></asp:Button>
                    <br />
                    <asp:Label runat="server" Forecolor="Red" ID="lbl" Text=" "></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                     </form>
                    </center>
            </div>
    </div> 
    </section>
</asp:Content>

