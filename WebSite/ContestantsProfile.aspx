﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminDashboard.master" AutoEventWireup="true" CodeFile="ContestantsProfile.aspx.cs" Inherits="ContestantProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
        <form id="formjug" class="form-horizontal" runat="server">
   
    <h3>Contestant's Registered</h3>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="ContestantId" DataSourceID="SqlDataSource1" Width="1037px">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ContestantName" HeaderText="Contestant Name" 
                    SortExpression="ContestantName" />
                <asp:BoundField DataField="ContactNo" HeaderText="Contact No" 
                    SortExpression="ContactNo" />
                <asp:BoundField DataField="Addres" HeaderText="Address" 
                    SortExpression="Addres" />
                <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" 
                    SortExpression="BirthDate" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="AreaOfInterest" HeaderText="AreaOfInterest" 
                    SortExpression="AreaOfInterest" />
                <asp:BoundField DataField="AadharCardno" HeaderText="AadharCardno" 
                    SortExpression="AadharCardno" />
                <asp:ImageField DataImageUrlField="Photograph">
                    <ControlStyle Height="100px" Width="100px" />
                    <ItemStyle Height="100px" Width="100px" />
                </asp:ImageField>
            </Columns>
        </asp:GridView>
        <br />
        <!--<asp:Button ID="Update" runat="server" Text="Update" /><br /><br />-->
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString2 %>" 
            DeleteCommand="DELETE FROM [Tbl_Contestant_Master] WHERE [ContestantId] = @ContestantId" 
            InsertCommand="INSERT INTO [Tbl_Contestant_Master] ([ContestantName], [ContactNo], [Addres], [LinkToCityId], [LinkToStateId], [Gender], [BirthDate], [Email], [AreaOfInterest], [AadharCardno], [Photograph]) VALUES (@ContestantName, @ContactNo, @Addres, @LinkToCityId, @LinkToStateId, @Gender, @BirthDate, @Email, @AreaOfInterest, @AadharCardno, @Photograph)" 
            SelectCommand="SELECT * FROM [Tbl_Contestant_Master]" 
            UpdateCommand="UPDATE [Tbl_Contestant_Master] SET [ContestantName] = @ContestantName, [ContactNo] = @ContactNo, [Addres] = @Addres, [LinkToCityId] = @LinkToCityId, [LinkToStateId] = @LinkToStateId, [Gender] = @Gender, [BirthDate] = @BirthDate, [Email] = @Email, [AreaOfInterest] = @AreaOfInterest, [AadharCardno] = @AadharCardno, [Photograph] = @Photograph WHERE [ContestantId] = @ContestantId">
            <DeleteParameters>
                <asp:Parameter Name="ContestantId" Type="Byte" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="ContestantName" Type="String" />
                <asp:Parameter Name="ContactNo" Type="String" />
                <asp:Parameter Name="Addres" Type="String" />
                <asp:Parameter Name="LinkToCityId" Type="Byte" />
                <asp:Parameter Name="LinkToStateId" Type="Byte" />
                <asp:Parameter Name="Gender" Type="Boolean" />
                <asp:Parameter DbType="Date" Name="BirthDate" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="AreaOfInterest" Type="String" />
                <asp:Parameter Name="AadharCardno" Type="String" />
                <asp:Parameter Name="Photograph" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ContestantName" Type="String" />
                <asp:Parameter Name="ContactNo" Type="String" />
                <asp:Parameter Name="Addres" Type="String" />
                <asp:Parameter Name="LinkToCityId" Type="Byte" />
                <asp:Parameter Name="LinkToStateId" Type="Byte" />
                <asp:Parameter Name="Gender" Type="Boolean" />
                <asp:Parameter DbType="Date" Name="BirthDate" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="AreaOfInterest" Type="String" />
                <asp:Parameter Name="AadharCardno" Type="String" />
                <asp:Parameter Name="Photograph" Type="String" />
                <asp:Parameter Name="ContestantId" Type="Byte" />
            </UpdateParameters>
        </asp:SqlDataSource>
   
     </form>
   </center>
</asp:Content>

