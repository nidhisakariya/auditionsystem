﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Verification.aspx.cs" Inherits="Verification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
	    		<h3>REGISTRATION</h3>
                    <center>
                    <form id="form1" runat="server">
                        OTP will be send on your mobile number and Email you specified:<br />

                        <asp:TextBox ID="Onetp" placeholder="Enter OTP" width="400px" runat="server"></asp:TextBox><br />
                       <asp:Button ID="verify" runat="server" Text="Verify" OnClick="verify_Click"></asp:Button><br />

                       <asp:Label ID="lblOTP" runat="server" Text=" "></asp:Label>
                    </form>
                   </center>
            </div>
         </div>
     </section>
     <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>
