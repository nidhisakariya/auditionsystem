﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class DisplayVideos : System.Web.UI.Page
{
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            n = Session["username"].ToString();
        }
        catch (NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Viewvideo")
        {
            Response.Redirect("ContestantVideo.aspx?ContestantId=" + e.CommandArgument);
        }
    }
}