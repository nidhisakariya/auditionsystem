﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminDashboard.master" AutoEventWireup="true" CodeFile="AssignTask.aspx.cs" Inherits="AssignTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server" id="Details" class="form-horizontal">
    <center>
    <h1>Details for uploading the task</h1><br /><br />
    Start Date:
    <asp:TextBox ID="sdate" Width="400px" class="form-control" TextMode="Date" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="sdate" runat="server" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator>
    <asp:Label runat="server" ForeColor="Red" ID="lblDateError" Text=" "></asp:Label><br />
    End Date:
    <asp:TextBox ID="edate" Width="400px" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="edate" runat="server" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator><br />
    <asp:CompareValidator ID="CompareValidator1" ControlToValidate="edate" ControlToCompare="sdate"  runat="server" ForeColor="Red" ErrorMessage="End date should be greater than start date!" Operator="GreaterThan"></asp:CompareValidator><br />
    Video Format:
    <asp:TextBox ID="format"  Width="400px" class="form-control" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="format" runat="server" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator><br />
    Video Time Duration:
    <asp:TextBox ID="duration" Width="400px" class="form-control" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="duration" runat="server" ForeColor="Red" ErrorMessage="Required!!"></asp:RequiredFieldValidator><br />
   
    Result Declaration Date:
    <asp:TextBox ID="result" Width="400px" class="form-control" TextMode="Date" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="Red" ErrorMessage="Required!" ControlToValidate="result"></asp:RequiredFieldValidator><br />
    <asp:CompareValidator ID="CompareValidator2" ForeColor="Red" runat="server" ErrorMessage="Result date should be greater than last date" Operator="GreaterThan" ControlToCompare="edate" ControlToValidate="result"></asp:CompareValidator>
   <br />
   <br />
    <asp:Button ID="Submit" runat="server" Text="Submit" onclick="Submit_Click" />&nbsp &nbsp 
        <input id="Reset1" type="reset" value="Reset" />    <br /><br /><br />
    <asp:Label ID="lblMessage" ForeColor="Red" runat="server" Text=" "></asp:Label>
    <br />
    <br />
    </center>
    </form>
</asp:Content>

