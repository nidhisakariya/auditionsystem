﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;

public partial class Results : System.Web.UI.Page
{
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    string rd;
    string user;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            user = Session["username"].ToString();
        }
        catch (NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }

        SqlConnection con = new SqlConnection(strConnString);
        try
        {
            com = new SqlCommand("RetrieveInfo", con);
            con.Open();

            SqlDataReader dr1 = com.ExecuteReader();

            while (dr1.Read())
            {
                rd = dr1["resultDate"].ToString();
            }
            con.Close();

           /* DateTime dt1 = DateTime.Today;
            DateTime dt2 = Convert.ToDateTime(rd);
            if(dt1==dt2)
            {*/
                string query = "select LinkToContestantId from Tbl_SelectedContestants";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                int i = 1;
                while (dr.Read())
                {
                    i++;
                }
                dr.Close();
                con.Close();
                if (i == 1)
                {
                    lbl.Visible = true;
                    declaredate.Text = rd;
                    GridView1.Visible = false;
                    pdf.Visible = false;
                }
                else
                {
                    lbl.Visible = false;
                    displayMarks();
                    pdf.Visible = true;
                }
            /*}
            else
            {
                declaredate.Text = rd;
                GridView1.Visible = false;
                pdf.Visible = false;
            }*/
        }
        catch (Exception exe)
        {
            lbl.Text = "Error!" + exe;
        }
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }
    protected void pdf_Click(object sender, EventArgs e)
    {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //To Export all pages
                    GridView1.AllowPaging = false;
                    this.BindGrid();

                    GridView1.RenderControl(hw);
                    StringReader sr = new StringReader(sw.ToString());
                    Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
            } 
    }
    private void BindGrid()
    {
        SqlConnection con = new SqlConnection(strConnString);
        string query = "select LinkToContestantId from Tbl_SelectedContestants";
        SqlCommand cmd = new SqlCommand(query, con);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dataTable = new DataTable();
        dataTable.Columns.Add("Serial no.");
        dataTable.Columns.Add("Contestant Name");
        dataTable.Columns.Add("Contestant email");
        int i = 1;
        while (dr.Read())
        {
            SqlConnection conn = new SqlConnection(strConnString);
            string q = "select ContestantName,Email from Tbl_Contestant_Master where ContestantId='" + dr["LinkToContestantId"] + "'";
            SqlCommand com1 = new SqlCommand(q, conn);
            conn.Open();
            SqlDataReader dr2 = com1.ExecuteReader();
            while (dr2.Read())
            {
                DataRow row = dataTable.NewRow();
                row["Serial no."] = i;
                row["Contestant Name"] = dr2["ContestantName"];
                row["Contestant email"] = dr2["Email"];
                dataTable.Rows.Add(row);
                i++;
                declaredate.Visible = false;
                lbl.Visible = false;
            }
            dr2.Close();
            conn.Close();
        }
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
        dr.Close();
        con.Close();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }
    public void displayMarks()
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);
            string query = "select AveragePoints,Reviews,JudgeId from Tbl_Results where VideoId=(select VideoId from Tbl_Videos where LinkToContestantId=(select ContestantId from Tbl_Contestant_Master where Email='" + user + "'))";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader dr3 = cmd.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("Serial no.");
            dataTable1.Columns.Add("Points");
            dataTable1.Columns.Add("Reviews");
            dataTable1.Columns.Add("Judge Name:");
            int i1 = 1;
            while (dr3.Read())
            {
                byte avg = Convert.ToByte(dr3["Average Points"]);
                string r = dr3["Reviews"].ToString();
                byte jid = Convert.ToByte(dr3["JudgeId"]);
                SqlConnection conn = new SqlConnection(strConnString);
                string q = "select JudgeName from Tbl_Judge_Master where JudgeId='"+ jid +"'";
                SqlCommand com1 = new SqlCommand(q, conn);
                conn.Open();
                SqlDataReader dr2 = com1.ExecuteReader();
                while (dr2.Read())
                {
                    DataRow row = dataTable1.NewRow();
                    row["Serial no."] = i1;
                    row["Points"] = avg;
                    row["Reviews"] = r;
                    row["Judge Name"] = dr2["JudgeName"];
                    dataTable1.Rows.Add(row);
                    i1++;
                }
                dr2.Close();
                conn.Close();
            }
            GridView2.DataSource = dataTable1;
            GridView2.DataBind();
            dr3.Close();
            con.Close();
        }
        catch (Exception ex)
        {
            lbl.Text = "Error!" + ex;
        }
    }
}