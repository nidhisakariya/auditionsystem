﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;

public partial class RegisterUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string n = Session["email"].ToString();
        uname.Text = n;
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    protected void Register1_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand com = new SqlCommand("UserRegistration", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@user", SqlDbType.VarChar)).Value = uname.Text.ToString();
            com.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar)).Value = password.Text.ToString();
            com.Parameters.Add(new SqlParameter("@type", SqlDbType.VarChar)).Value = "Contestant";
            con.Open();
            int k = com.ExecuteNonQuery();
            if (k != 0)
            {
                Response.Redirect("Login.aspx");
            }
            con.Close();
        }
        catch(Exception ex)
        {
            lbl.Text = ex.ToString();
        }
    }
    static string Encrypt(string value)
    {
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(value));
            return Convert.ToBase64String(data);
        }
    }
    protected void password_TextChanged(object sender, EventArgs e)
    {
        password.Text = Encrypt(password.Text);
    }
    protected void retypepassword_TextChanged(object sender, EventArgs e)
    {
        retypepassword.Text = Encrypt(retypepassword.Text);
    }
}