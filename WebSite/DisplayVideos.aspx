﻿<%@ Page Title="" Language="C#" MasterPageFile="~/JudgeDashboard.master" AutoEventWireup="true" CodeFile="DisplayVideos.aspx.cs" Inherits="DisplayVideos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="heading" >
		<h1>VIDEOS</h1>
    </div>
    <section id="main" class="wrapper">
    <div class="inner">
			<div class="content">
                    <center>
                    <form id="videos">
                   <br />
                    <asp:DataList ID="DataList1" runat="server" DataKeyField="ContestantId" 
                        DataSourceID="SqlDataSource1" RepeatColumns="3" 
                        RepeatDirection="Horizontal" onitemcommand="DataList1_ItemCommand">
                        <ItemTemplate>
                            <table class="style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ContestantName") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" Width="400" Height="500px" runat="server" ImageUrl='<%# Eval("Photograph") %>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" CommandName="Viewvideo" 
                                            Text="View Video" Width="200" CommandArgument='<%# Eval("ContestantId") %>'/>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                   <br />
                   <br />
                   <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString7 %>" 
                        SelectCommand="SELECT * FROM [Tbl_Contestant_Master]"></asp:SqlDataSource>
                   <br />
                    <asp:Label ID="lbl_Message" runat="server" Text=" "></asp:Label>
                   <br />
                   <br /> 
                   <br />
                   </form>
                   </center>
                </div>
        </div> 
        </section>
</asp:Content>

