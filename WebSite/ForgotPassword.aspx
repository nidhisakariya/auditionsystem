﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
	    		<h3>REGISTRATION</h3>
                <center>
    <form id="fgp" runat="server">
    <br />
    Email Id:
    <asp:TextBox runat="server" ID="email" ></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" ControlToValidate="email" runat="server" ErrorMessage="Required!"></asp:RequiredFieldValidator><br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="email" runat="server" ForeColor="Red" ErrorMessage="Invalid email" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator><br />
    <br />
    <asp:Button runat="server" ID="btnsubmit" Text="Send OTP" 
        onclick="btnsubmit_Click"/>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    </form>
    </center>
        </div>
         </div>
     </section>
     <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>
