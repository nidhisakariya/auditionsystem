﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class changePassword : System.Web.UI.Page
{
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
         try
        {
            n = Session["username"].ToString();
        }
        catch (NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand cmd;
    protected void Cpassword_TextChanged(object sender, EventArgs e)
    {
        Cpassword.Text = Encrypt(Cpassword.Text);
    }
    protected void Npassword_TextChanged(object sender, EventArgs e)
    {
        Npassword.Text = Encrypt(Npassword.Text);
    }
    protected void rpassword_TextChanged(object sender, EventArgs e)
    {
        rpassword.Text = Encrypt(rpassword.Text);
    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        string pswd = " ";
        SqlConnection con = new SqlConnection(strConnString);
        string q = "select UserPassword from Tbl_users where Username='" + n + "'";
        cmd = new SqlCommand(q, con);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            pswd = dr["UserPassword"].ToString();
        }
        dr.Close();
        con.Close();
        if (pswd.ToString() == Cpassword.Text)
        {

            string q1 = "update Tbl_users set UserPassword='" + Npassword.Text + "' where UserName='"+n+"'";
            cmd = new SqlCommand(q1, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i != 0)
            {
                label.Text = "Password changed successfully!";
            }
        }
        else
        {
            label.Text = "Current Password is incorrect!";
        }
    }
    static string Encrypt(string value)
    {
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(value));
            return Convert.ToBase64String(data);
        }
    }
}