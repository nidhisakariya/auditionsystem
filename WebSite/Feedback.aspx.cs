﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Feedback : System.Web.UI.Page
{
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            n = Session["username"].ToString();
        }
        catch (NullReferenceException)
        {
            Response.Redirect("Login.aspx");
        }
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    byte uid;
    protected void btnFeedback_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);

            string query = "select ContestantId from Tbl_Contestant_Master where Email='" + n + "'";
            com = new SqlCommand(query, con);
            con.Open();
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                uid = Convert.ToByte(dr["ContestantId"]);
            }
            con.Close();

            string fb = Convert.ToString(txtfeedback.Text);
            string q = "insert into Tbl_Feedback(Feedback,ContestantId) values('" + fb + "','" + uid + "')";
            com = new SqlCommand(q, con);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();

            if (i == 1)
            {
                lbl.Text = "Thank you for your valuable feedback!";
            }
            txtfeedback.Text = " ";
        }
        catch (Exception exe)
        {
            lbl.Text = "Error!" + exe;
        }
    }
}