﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

public partial class Login : System.Web.UI.Page
{
    int cnt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["username"] = txt_uname.Text;
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    protected void btn_login_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("VerifyUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar)).Value = txt_uname.Text.ToString();
            cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.VarChar)).Value = txt_password.Text.ToString();

            IDbDataParameter ctype = cmd.CreateParameter();
            ctype.ParameterName = "@type";
            ctype.Direction = System.Data.ParameterDirection.Output;
            ctype.DbType = System.Data.DbType.String;
            ctype.Size = 12;
            cmd.Parameters.Add(ctype);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            string type = ctype.Value.ToString();
                if (type.Equals("Contestant"))
                {
                    Response.Redirect("Home.aspx");
                }
                else if (type.Equals("Judge"))
                {
                    Response.Redirect("JudgeHome.aspx");
                }
                else if (type.Equals("Admin"))
                {
                    Response.Redirect("AdminHome.aspx");
                }
            else
            {
                lbl_message.Text = "Invalid username or password!";
            }
        }
        catch (Exception ex)
        {
            lbl_message.Text = "Error" + ex;
        }
    }
    protected void Link_Click(object sender, EventArgs e)
    {
        Response.Redirect("Registration.aspx");
    }
    protected void Forgot_Click(object sender, EventArgs e)
    {
        Response.Redirect("ForgotPassword.aspx");
    }
    static string Encrypt(string value)
    {
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(value));
            return Convert.ToBase64String(data);
        }
    }
    protected void txt_password_TextChanged(object sender, EventArgs e)
    {
        txt_password.Text = Encrypt(txt_password.Text);
    }
}