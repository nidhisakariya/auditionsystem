﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProfilePage.aspx.cs" Inherits="ProfilePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: darkred;
  color: white;
}

.topnav-right {
  float: right;
}
    .style1
    {
        width: 100%;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="topnav">
                     <div class="topnav-right">
                            <a href="changePassword.aspx">Change Password</a>&nbsp;
                            <a href="EditProfile.aspx">Edit Profile</a>
                     </div>
                    </div>
     <div id="heading" >
				<h1>PROFILE </h1>
    </div>
    <section id="main" class="wrapper">
                    <div class="inner">
			        <div class="content">
                    <center>
                    <form id="profile">
                    <br />
                    <br />
                    <br />
                    <asp:DataList ID="DataList1" runat="server" DataKeyField="ContestantId" 
                        DataSourceID="SqlDataSource1">
                        <ItemTemplate>
                            <table class="style1">
                                <tr>
                                    <td>
                                        &nbsp;<asp:Label ID="Label1" runat="server" Text='<%# Eval("ContestantName") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" Width="400px" runat="server" ImageUrl='<%# Eval("Photograph") %>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("ContactNo") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("Addres") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" TextMode="Date" runat="server" Text='<%# Eval("BirthDate") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("AreaOfInterest") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("AadharCardno") %>'></asp:Label>
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString10 %>" 
                        SelectCommand="SELECT * FROM [Tbl_Contestant_Master] WHERE ([Email] = @Email)">
                        <SelectParameters>
                            <asp:SessionParameter Name="Email" SessionField="username" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                    </form>
                   </center>
                </div>
        </div> 
        </section>
</asp:Content>