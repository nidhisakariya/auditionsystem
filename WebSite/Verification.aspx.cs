﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


public partial class Verification : System.Web.UI.Page
{
    String check_otp;
    protected void Page_Load(object sender, EventArgs e)
    {
        check_otp = Session["otp"].ToString();
        lblOTP.Text = check_otp.ToString();
    }
    protected void verify_Click(object sender, EventArgs e)
    {
        if(Onetp.Text==check_otp)
        {
            Response.Redirect("RegisterUser.aspx");
        }
        else
        {
            lblOTP.Text = "Invalid";
        }
    }
}