﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class JudgeDashboard : System.Web.UI.MasterPage
{
    string n;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            n = Session["username"].ToString();
            btn_log.Text = "Logout";
        }
        catch (NullReferenceException)
        {
            //Response.Redirect("Home.aspx");
        }
    }
    protected void btn_log_Click(object sender, EventArgs e)
    {
        if (btn_log.Text == "Login")
        {
            Response.Redirect("Login.aspx");
        }
        else
        {
            Session.Abandon();
            Response.Redirect("Home.aspx");
        }
    }
}
