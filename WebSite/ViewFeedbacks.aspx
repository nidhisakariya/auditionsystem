﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminDashboard.master" AutoEventWireup="true" CodeFile="ViewFeedbacks.aspx.cs" Inherits="ViewFeedbacks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
    <form id="viewfeedback" runat="server">
<br />
<br />
<br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="4" DataKeyNames="FeedbackId" DataSourceID="SqlDataSource1" 
        onrowdeleted="GridView1_RowDeleted1" Width="386px">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:BoundField DataField="Feedback" HeaderText="Feedback" 
                SortExpression="Feedback" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
<br />
<br />
<br />
<br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AuditionSystemConnectionString3 %>" 
        SelectCommand="SELECT * FROM [Tbl_Feedback]" 
        DeleteCommand="DELETE FROM [Tbl_Feedback] WHERE [FeedbackId] = @FeedbackId" 
        InsertCommand="INSERT INTO [Tbl_Feedback] ([Feedback], [ContestantId]) VALUES (@Feedback, @ContestantId)" 
        UpdateCommand="UPDATE [Tbl_Feedback] SET [Feedback] = @Feedback, [ContestantId] = @ContestantId WHERE [FeedbackId] = @FeedbackId">
        <DeleteParameters>
            <asp:Parameter Name="FeedbackId" Type="Byte" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Feedback" Type="String" />
            <asp:Parameter Name="ContestantId" Type="Byte" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Feedback" Type="String" />
            <asp:Parameter Name="ContestantId" Type="Byte" />
            <asp:Parameter Name="FeedbackId" Type="Byte" />
        </UpdateParameters>
    </asp:SqlDataSource>
<br />
<br />
    </form>
</center>
</asp:Content>

