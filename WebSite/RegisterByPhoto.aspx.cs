﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class RegisterByPhoto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    string ans;

    public void sendsms(string content, string number)
    {
        string destinationaddr = "91" + number;
        string message = "Hey dancer! Your OTP for registering with Dance Out Loud is" + content;

        String message1 = HttpUtility.UrlEncode(message);
        using (var wb = new WebClient())
        {
            byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "UXj3X4D66II-R6WeadtXjuGpGi0Nr45eDqL4ItX5ES"},
                {"numbers" , destinationaddr},
                {"message" , message1},
                {"sender" , "TXTLCL"}
                });
            string result = System.Text.Encoding.UTF8.GetString(response);
        }
    }
    public void sendEmail(string content, string mailaddress)
    {
        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        mail.To.Add(mailaddress);
        mail.From = new MailAddress("17bmiit141@gmail.com", "Audition Magic", System.Text.Encoding.UTF8);
        mail.Subject = "OTP for Dance Out Loud";
        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        mail.Body = "Hey dancer! Your OTP to register with Dance Out Loud is:" + content;
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential("17bmiit141@gmail.com", "shreenathji");
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception ex)
        {
            Exception ex2 = ex;
            string errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }
        }
    }
    public String otpgeneration()
    {
        string numbers = "1234567890";
        string characters = numbers;
        int length = 6;
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;
    }
    string strConnString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    SqlCommand com;
    protected void upload_Click(object sender, EventArgs e)
    {
        try
        {
            string n = Session["email"].ToString();

            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("GetDetail", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = n;

            IDbDataParameter cid = cmd.CreateParameter();
            cid.ParameterName = "@id";
            cid.Direction = System.Data.ParameterDirection.Output;
            cid.DbType = System.Data.DbType.Byte;
            cmd.Parameters.Add(cid);
            
            IDbDataParameter cnum = cmd.CreateParameter();
            cnum.ParameterName = "@num";
            cnum.Direction = System.Data.ParameterDirection.Output;
            cnum.DbType = System.Data.DbType.String;
            cnum.Size = 50;
            cmd.Parameters.Add(cnum);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            
            byte uid = Convert.ToByte(cid.Value);
            string number = cnum.Value.ToString();
            ans = otpgeneration();
            sendEmail(ans, n);
            //sendsms(ans,number);
            //lbl.Text = uid.ToString()+" "+number.ToString();
            Session["otp"] = ans.ToString();

            FileUpload img = (FileUpload)imgupload;
            Byte[] imgByte = null;
            if (img.HasFile && img.PostedFile != null)
            {
                //To create a PostedFile
                HttpPostedFile File = imgupload.PostedFile;
                //Create byte Array with file len
                imgByte = new Byte[File.ContentLength];
                //force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength);
            }

            FileUpload imgaadhar = (FileUpload)aadharupload;
            Byte[] imgByteaadhar = null;
            if (imgaadhar.HasFile && imgaadhar.PostedFile != null)
            {
                //To create a PostedFile
                HttpPostedFile File = aadharupload.PostedFile;
                //Create byte Array with file len
                imgByteaadhar = new Byte[File.ContentLength];
                //force the control to load data in array
                File.InputStream.Read(imgByteaadhar, 0, File.ContentLength);
            }

            com = new SqlCommand("UploadImage", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@uid", uid);
            com.Parameters.AddWithValue("@userimage", imgByte);
            com.Parameters.AddWithValue("@aadharimage", imgByteaadhar);

            con.Open();
            int k = com.ExecuteNonQuery();
            if (k != 0)
            {
                Response.Redirect("Verification.aspx");
            }
            con.Close();
        }
        catch (Exception exe)
        {
            lbl.Text = exe.ToString();
        }
    }
}