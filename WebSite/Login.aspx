﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
    <div id="header">
		<a class="logo" href="Home.aspx">Audition Magic</a>
	</div>
    <section id="main" class="wrapper">
		<div class="inner">
			<div class="content">
					<h3>LOGIN</h3>
                    <center>
                    <form id="form1" runat="server">
                            Username:
                         <asp:TextBox ID="txt_uname" runat="server" Width="400px"></asp:TextBox><br />
                        Password:
                         <asp:TextBox ID="txt_password" Textmode="Password" runat="server" Width="400px" 
                                ontextchanged="txt_password_TextChanged"></asp:TextBox><br />
                           <asp:Button ID="btn_login" runat="server" Text="Login" onclick="btn_login_Click" />
                         <br />
                         <br/>
                         <asp:LinkButton ID="Link" runat="server" ForeColor="Black" onclick="Link_Click">Not Registered yet?? Register now!!</asp:LinkButton><br />
                         <asp:LinkButton ID="Forgot" runat="server" ForeColor="Black" onclick="Forgot_Click">Forgot Password</asp:LinkButton><br />
                         <asp:Label ID="lbl_message" ForeColor="Red"  runat="server" Text="   "></asp:Label>
                   </form>
                   </center>
            </div>
         </div>
     </section>
     <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>
